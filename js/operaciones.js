// Variables globales
const tipoCambio = {
	USD: 20.0,
	MXN: 1.0,
	CAD: 15.0,
	EUR: 25.0,
  };
  
  // Funciones
  function convertirMoneda(cantidad, monedaOrigen, monedaDestino) {
	const tipoCambioOrigen = tipoCambio[monedaOrigen];
	const tipoCambioDestino = tipoCambio[monedaDestino];
  
	const cantidadDolares = cantidad / tipoCambioOrigen;
	const cantidadDestino = cantidadDolares * tipoCambioDestino;
  
	return cantidadDestino;
  }
  
  function agregarRegistro(monedaOrigen, monedaDestino, tipoCambio, montoOriginal, montoConvertido) {
	const registros = document.querySelector("#registros");
  
	const fila = document.createElement("tr");
	fila.innerHTML = `
	  <td>${monedaOrigen}</td>
	  <td>${monedaDestino}</td>
	  <td>${tipoCambio}</td>
	  <td>${montoOriginal}</td>
	  <td>${montoConvertido}</td>
	`;
  
	registros.appendChild(fila);
  }
  
  // Event listeners
  document.querySelector("#calcular").addEventListener("click", () => {
	const monedaOrigen = document.querySelector("#moneda-origen").value;
	const monedaDestino = document.querySelector("#moneda-destino").value;
	const cantidad = Number(document.querySelector("#cantidad").value);
  
	const montoConvertido = convertirMoneda(cantidad, monedaOrigen, monedaDestino);
	const montoOriginal = cantidad.toFixed(2);
  
	const importePagarInput = document.querySelector("#importe-pagar");
	importePagarInput.value = montoConvertido.toFixed(2);
  
	agregarRegistro(monedaOrigen, monedaDestino, tipoCambio[monedaDestino], montoOriginal, montoConvertido.toFixed(2));
  });